module.exports = {
    assetPrefix: process.env.NODE_ENV === 'production' ? `/${process.env.CI_PROJECT_NAME}` : '',
};