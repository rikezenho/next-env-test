import Head from 'next/head'
import styles from '../styles/Home.module.css'

export async function getStaticProps(context) {
  const { VARIABLE_WITHOUT_PUBLIC_BUT_INJECTED_BY_STATIC_PROPS } = process.env;
  
  return {
    props: {
      VARIABLE_WITHOUT_PUBLIC_BUT_INJECTED_BY_STATIC_PROPS,
    }, // will be passed to the page component as props
  }
}

export default function Home(props) {
  const {
    VARIABLE_WITHOUT_PUBLIC_BUT_INJECTED_BY_STATIC_PROPS,
  } = props;

  return (
    <div className={styles.container}>
      <Head>
        <title>Env Variables Test</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Env Variables Test
        </h1>
        <ul>
          <li>NEXT_PUBLIC_TEST_VARIABLE: {process.env.NEXT_PUBLIC_TEST_VARIABLE}</li>
          <li>NEXT_PUBLIC_CI_VARIABLE: {process.env.NEXT_PUBLIC_CI_VARIABLE}</li>
          <li>VARIABLE_WITHOUT_PUBLIC_BUT_INJECTED_BY_STATIC_PROPS: {VARIABLE_WITHOUT_PUBLIC_BUT_INJECTED_BY_STATIC_PROPS}</li>
        </ul>
      </main>
    </div>
  )
}
